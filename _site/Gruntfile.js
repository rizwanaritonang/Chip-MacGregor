/* ------------------------------------------------------------------------- *
 * GLOBALS MODULE, REQUIRE
/* ------------------------------------------------------------------------- */
module.exports = function(grunt) {

	"use strict";

  
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),


		/* UGLIFY CONFIGURATION
		/* ------------------------------------ */
		uglify: {
			global: {
				files: {
					"js/scripts.min.js": ["js/scripts.js"]
				}
			}
		},

		/* AUTOPREFIXER CONFIGURATION
		/* ------------------------------------ */
		autoprefixer: {
			global: {
				src: "css/app-unprefixed.css",
				dest: "css/app.css"
			}
		},

		/* SASS CONFIGURATION
		/* ------------------------------------ */
	    sass: {
	    	global: {
	    		options: {
	    			style: "expaned"
	    		},
	    		files: {
	    			"css/app-unprefixed.css": "sass/application.scss"
	    		}
	    	}
	    },

	    /* JSHINT CONFIGURATION
	    /* ------------------------------------ */
	    jshint: {
	    	options: {
	    		force: true
	    	},
	    	all: ['js/scripts.js'],
	    },

	    /* SHELL CONFIGURATION
	    /* ------------------------------------ */
	    shell : {
	        jekyllBuild : {
	            command : 'jekyll build --config _config-dev.yml'
	        },
	        jekyllServe : {
	            command : 'jekyll serve --baseurl'
	        },
	    },

	    /*  PRETTIFY CONFIGURATION
	    /* ------------------------------------ */
	    prettify: {
	    	options: {
		    	indent: 4,
		    	wrap_line_length: 80,
		    	brace_style: 'expand'
	    	},
	    	all: {
	    		files: [
	    			{
	    				expand: true,
	    				src: ["_site/*.html", "_site/**/*.html"],
	    				ext: '.html'
	    			}
	    		]
	    	}
	    },

    	/*  WATCH CONFIGURATION
    	/* ------------------------------------ */
	    watch: {

	    	options: {
	    		livereload: true,
	    	},

			site: {
				files: ["index.html", "writing.html", "about.html", "_layouts/*.html", "_posts/*.md", "_projects/*.md", "_includes/*.html"],
				tasks: ["shell:jekyllBuild"]
			},

	    	gruntfile: {
	    		files: "Gruntfile.js",
	    		tasks: ["jshint:gruntfile"],
	    	},

	    	scripts: {
	    		files: ["js/scripts.js"],
	    		tasks: ["jshint", "uglify", "shell:jekyllBuild"],
	    	},

	    	css: {
	    		files: "sass/*.scss",
	    		tasks: ["sass", "autoprefixer", "shell:jekyllBuild"],
	    	},

	    	html: {
	    		files: "_site/*.html",
	    		tasks: ["prettify:all", "shell:jekyllBuild"]
	    	},
	    } // watch

  });


  require("load-grunt-tasks")(grunt);

  // register custom grunt tasks
  grunt.registerTask( "serve", ["shell:jekyllServe"] );
  grunt.registerTask( "default", ["sass", "autoprefixer", "prettify" ,"shell:jekyllBuild", "watch" ] );
};
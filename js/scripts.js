(function ($) {
	'use strict';

	// BANNER-CAROUSEL
	$(".banner-carousel").owlCarousel({
		mouseDrag: true,
		animateOut: 'fadeOut',
		items: 1,
		touchDrag: true,
		loop: true,
		dotsEach: true,
		responsive: true,
		autoplay: false,
		autoplayTimeout: 3000,
	});

	// MAP
    $(window).on("load", function() { 

        //google map load
        $('#map').gmap({
            'center': '-6.91049437732718,107.625922129783',
            'zoom': 15,
            scrollwheel: false,
            'disableDefaultUI': false,
            draggable: false,
            'styles': [{
                stylers: [{
                    lightness: 7
                }, {
                    saturation: -100
                }]
            }],
            'callback': function() {
                var self = this;
                self.addMarker({
                        'position': this.get('map').getCenter(),
                        icon: '../images/office-building.png',
                    });
            }
        });
    });


	// CLONE NAVIGATION TO RWD-NAVIGATION-SIDEBAR
	$(".main-navigation > ul").clone(false).find("ul, li").removeAttr("id").remove(".submenu").appendTo($(".rwd-navigation-first > ul"));

	$(".main-navigation > ul").clone(false).find("ul, li").removeAttr("id").remove(".submenu").appendTo($(".rwd-navigation-second > ul"));
	$(".mobile-nav").on('show.bs.collapse', function(){
		$("body").on( 'click', function() {
			$(".mobile-nav").collapse("hide");
		});
	});

    $(".box-category > ul").clone(false).find("ul, li").removeAttr("id").remove(".submenu").appendTo($(".rwd-box-category > ul"));
    $(".btn-rwd-category").click( function(e) {
        $(".rwd-box-category ul").slideToggle();
    });

})(jQuery);